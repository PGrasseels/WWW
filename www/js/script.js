  loadServer();

  $('.alert').click(function(){
    getInfos($(this).attr('data-id'), $(this).attr('id'));
    return false;
  });

  $('#saveNew').click(function(){
    hide();
    
    if($('#serverName').val().length < 4){
      $('#serverName').next('.alert').show();
    }else{

      $.ajax({
          url: $('#serverUrl').val()+"core/install.php", 
          type: 'post',
          error: function(XMLHttpRequest, textStatus, errorThrown){
            $('#errorHost').html(textStatus + " : " + XMLHttpRequest.status + " - Server not install or bad url. (install)").show();
          },
          success: function(data){
            if(data){
              $('#serverUrl').next('.icon-ok').show();

              $.ajax({
                url: $('#serverUrl').val()+"core/auth.php", 
                data: { 'password' : $('#serverPassword').val() },
                type: 'post',
                error: function(XMLHttpRequest, textStatus, errorThrown){
                    $('#errorHost').html(textStatus + " : " + XMLHttpRequest.status + " - Server not install or bad url.").show();
                },
                success: function(data) {
                data = JSON.parse(data);
                if(data['error'] == true){
                    $('#passwordError').html(data['errorMessage']).show();
                  }else{
                    $('#serverPassword').next('.icon-ok').show();
                    addServer($('#serverUrl').val(),$('#serverName').val());
                    $('#newServer').modal('toggle');
                    clearInput();
                    hide();
                  }
                }
              });

            }else{
              $('#serverUrl').next('.icon-ok').next('.icon-remove').show();
            }
          }
      });
      $('#serverName').next('.alert').hide();
    }
  });

  $('#removeAll').click(function(){
    localStorage.clear();
    loadServer();
    return false;
  });

  function getInfos(id, div){
    var serverList = JSON.parse(localStorage.getItem("serverList"));
    $('#serverInfo').html(
      ' <a href="" class="btn btn-success pull-right">Edit</a> ' +
      ' <a href="" class="btn btn-danger pull-right" style="margin-right:20px;">Delete</a> ' +
      '<span>Server name : ' + serverList[id].name + '<span><br />' +
      '<span>Server host : ' + serverList[id].host + '<span><br />' +
      '<hr>' + getSys(serverList[id]) +getCpu(serverList[id])
      );
    $('.alert').removeClass('alert-warning');
    $('#'+div).addClass('alert-warning');
  }

  function hide(){
    $('#serverUrl').next('.icon-ok').hide();
    $('#serverUrl').next('.icon-ok').next('.icon-remove').hide();
    $('#serverPassword').next('.icon-ok').hide();
    $('#serverPassword').next('.icon-ok').next('.icon-remove').hide();
    $('#passwordError').hide();
    $('#errorHost').hide();
  }

  function addServer(_host, _name){
    var list = JSON.parse(localStorage.getItem("serverList"));
    if(list == null){
      list = [{  
          host: _host,
          name: _name
        }];
    }else{
      list.push({
        host: _host,
        name: _name
      });
    }

    localStorage.setItem('serverList', JSON.stringify(list));
    loadServer();
  }

  function loadServer(){
    $('#serverlist').empty();

    var serverList = JSON.parse(localStorage.getItem("serverList"));
    if(serverList != null){
      if(localStorage.getItem('id') == null){
        localStorage.setItem('id', 0);
      }

      for(var i = 0; i < serverList.length; i++){
        $('#serverlist').append('<a href=""><div class="alert alert-info" id="server_'+ i +'" data-id="' + i + '">' + serverList[i].name + '</div></a>');
      }
    }
  }

  function clearInput(){
    $('#serverName').val('');
    $('#serverUrl').val('');
    $('#serverPassword').val('');
  }

  function getCpu(server){
    var infos = '<h3>CPU</h3>';
    $.ajax({
      url: server.host + 'core/request.php',
      data: { 'request' : 'getCpu()' },
      type: 'post',
      async: false,
      success: function(data){
        data = JSON.parse(data);
      },
    });
    infos += '<hr>';
    return infos;
  }

  function getSys(server){
    var infos = '<h3>System info</h3>';
    $.ajax({
      url: server.host + 'core/request.php',
      async: false,
      data: { 'request' : 'getSys()' },
      type: 'post',
      success: function(data){
        data = JSON.parse(data);
        infos += 'Os : ' + data['os'] + '<br />';
        infos += 'Kernel : ' + data['kernel'] + '<br />';
        infos += 'Uptime : ' + data['uptime'];
      }
    });
    infos += '<hr>';
    return infos;
  }
